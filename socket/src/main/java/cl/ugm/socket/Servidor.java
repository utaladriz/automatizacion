package cl.ugm.socket;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {
    public static void main(String[] args) throws Exception {
        ServerSocket srv = new ServerSocket(9000);
        System.out.println("Servidor is running");
        System.out.println("Waiting for a client");
        Socket s = null;
        while( (s = srv.accept()) != null) {
           Thread thread = new Thread(new Trabajador(s));
           System.out.println("Worker created");
           thread.start();
           System.out.println("Worker started");
        }
        System.out.println("The end");
    }
}
