package cl.ugm.socket;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Trabajador implements Runnable{
    private Socket socket;

    public Trabajador(Socket socket){
        this.socket = socket;
    }

    public void run() {
        Boolean running = true;
        while (running) {
            try {
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                Object msg = ois.readObject();
                if (msg instanceof Mensaje) {
                    Mensaje mensaje = (Mensaje) msg;
                    System.out.println("Server received message " + mensaje.getNombre());
                    mensaje.setSaludo("Hello " + mensaje.getNombre());
                    ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                    oos.writeObject(mensaje);
                    System.out.println("Server send response " + mensaje.getSaludo());
                } else if (msg instanceof Fin) {

                        running = false;
                    }


            } catch (Exception ex) {
                System.out.println("Error " + ex.getMessage());
                running = false;
            }
        }
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Worker stopped");

    }
}
