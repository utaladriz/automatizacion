package cl.ugm.socket;

import java.io.Serializable;

public class Mensaje implements Serializable {
    private String nombre;
    private String saludo;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSaludo() {
        return saludo;
    }

    public void setSaludo(String saludo) {
        this.saludo = saludo;
    }


}
