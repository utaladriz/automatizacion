package cl.ugm.socket;

import java.io.*;
import java.net.Socket;

public class Cliente {
    public static void main(String[] args) throws Exception {
        Socket socket = null;
        Boolean connected = false;
        while (!connected){
          try {
              socket = new Socket("localhost",9000);
              connected = true;
          }
          catch(Throwable ex){
              System.out.println("Server is down");
              Thread.sleep(3000);
          }
        }

        System.out.println("Client connected");
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String msg = reader.readLine();
        System.out.println(msg);

         while(!msg.equals("fin")) {
            ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
            Mensaje mensaje = new Mensaje();
            mensaje.setNombre(msg);
            System.out.println("Sending message to server " + mensaje.getNombre());
            oos.writeObject(mensaje);
            ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
            mensaje = (Mensaje) ois.readObject();
            System.out.println("Server answer " + mensaje.getSaludo());
            msg = reader.readLine();
        }
        ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
        oos.writeObject(new Fin());
        socket.close();
        System.out.println("Client stopped");

    }
}
