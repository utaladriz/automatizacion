namespace java cl.ugm.ejemplothrift
namespace python ejemplothrift

typedef i32 int

service CalculadoraSvc
{
   int multiplicar(int a, int b),
   int sumar(int a, int b)
}