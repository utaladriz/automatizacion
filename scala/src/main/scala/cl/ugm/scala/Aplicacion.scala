package cl.ugm.scala

import cl.ugm.otro.Objeto.mirar


object Persona {
  def apply(edad:Int, nombre:String) = new Persona(edad, nombre)
  def imprimir(p : Persona): Unit ={
    println(s"Persona(${p.edad},${p.nombre}")
  }
}

class Persona(var edad : Int, var nombre : String)

object Aplicacion extends App {
  println("Hola Mundo");
  val p = Persona(10,"Juan")
  println(p)
  mirar(p)

}
