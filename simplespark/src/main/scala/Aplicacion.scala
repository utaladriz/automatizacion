import org.apache.spark.sql.SparkSession


  object Aplicacion {
    def main(args: Array[String]) {
      val logFile = "/Users/utaladriz/aplicaciones/spark-2.4.0-bin-hadoop2.7/README.md"
      val spark = SparkSession.builder.appName("Aplicacion").getOrCreate()
      val logData = spark.read.textFile(logFile).cache()
      val numeroAs = logData.filter(linea => linea.contains("a")).count()
      val numeroBs = logData.filter(linea => linea.contains("b")).count()
      println(s"Lineas con a: $numeroAs, lineas con b: $numeroBs")
      spark.stop()
    }
  }


