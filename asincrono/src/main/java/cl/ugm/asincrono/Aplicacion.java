package cl.ugm.asincrono;

import java.util.concurrent.CompletableFuture;

public class Aplicacion {


    public static Integer  tareaMuyLarga() {
        System.out.println("Inicio Tarea Larga");
        int j = 0;
        for(int i =0; i < 100;i=i+1){
            try {
                Thread.sleep(100);
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
            if (i == 90)
                throw new RuntimeException("Error provocado i = 90");
            j = i;
        }
        System.out.println("Fin Tarea Larga "+j );
        return j;

    }
    public static void main(String[] args) throws Exception {
        long inicio = System.currentTimeMillis();
        System.out.println(inicio);

        CompletableFuture
                .supplyAsync(()->tareaMuyLarga())
                .thenAccept(res->System.out.println("Volví de la llamada asíncrona resultado "+res));

        long fin = System.currentTimeMillis();
        System.out.println(fin);
        System.out.println((fin-inicio)/1000+ " segundos");
        Thread.sleep(12000);

    }
}
