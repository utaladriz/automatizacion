package cl.ugm.serializacion;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class Aplicacion {



    public static void main(String[] args) throws Exception {
        FileOutputStream out = new FileOutputStream("personas.dat");
        ObjectOutputStream opOut = new ObjectOutputStream(out);

        Persona persona;
        persona = new Persona();
        persona.nombre="Ronaldo";
        persona.edad = 45;

        opOut.writeObject(persona);

        persona = new Persona();
        persona.nombre="Mathieu";
        persona.edad = 23;

        opOut.writeObject(persona);

        opOut.close();
        out.close();



    }

}
