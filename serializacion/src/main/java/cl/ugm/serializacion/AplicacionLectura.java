package cl.ugm.serializacion;

import java.io.FileInputStream;
import java.io.ObjectInputStream;

public class AplicacionLectura {
    public static void main(String[] args) throws Exception {
        FileInputStream in = new FileInputStream("personas.dat");
        ObjectInputStream is = new ObjectInputStream(in);


        Object o = is.readObject();

        if (o instanceof Persona)
            System.out.println(((Persona) o).nombre);

        o = is.readObject();
        if (o instanceof Persona)
            System.out.println(((Persona) o).nombre);


    }
}
