package cl.ugm.restasync;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class Servicio {

    @Async
    public void tareaMuyLarga(Integer idTicket, String urlCallback){
        System.out.println("Tarea larga idTicket "+idTicket);
        try {
            Thread.sleep(15000);
        }
        catch (Exception ex){
            ex.printStackTrace();
        }
        System.out.println("Fin tarea larga");
        Controlador.tareas.remove(idTicket);
        RestTemplate restTemplate = new RestTemplate();
        String respuestaCallback = restTemplate.getForObject(urlCallback+"?idTicket="+idTicket, String.class);
        System.out.println("Respuesta del callback "+respuestaCallback );
    }
}
