package cl.ugm.restasync;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.Set;

@RestController
public class Controlador {
    static Set tareas = new HashSet();

    @Autowired
    Servicio servicio;

    @GetMapping("/ejecutar")
    public Integer ejecutar(@RequestParam(name="tarea") String tarea, @RequestParam(name="urlcallback") String urlCallback){
        Integer idTicket = new Double((Math.random()*1000)).intValue();
        tareas.add(idTicket);
        servicio.tareaMuyLarga(idTicket, urlCallback);
        return idTicket;
    }

    @GetMapping("/callback")
    public String callback(@RequestParam(name="idTicket") Integer idTicket){
        System.out.println("Me avisaron que la tarea "+idTicket+" terminó");
        return "OK";
    }

    @GetMapping("/monitoreo")
    public String monitoreo(@RequestParam(name="idTicket") Integer idTicket){
           if (tareas.contains(idTicket))
               return "Ejecutándose";
           else
               return "Finalizada";

    }

}
