package cl.ugm.restjson;

import org.springframework.web.bind.annotation.*;

@RestController
public class Controlador {
    @GetMapping("/hola")
    public String saludar(@RequestParam(name="nombre") String nombre){
        return "Hola "+nombre;
    }

    @GetMapping("/cliente")
    public Cliente cliente(){
        return new Cliente("Mathieu", 23);
    }

    @PostMapping("/cliente")
    public Cliente cliente(@RequestBody Cliente cliente){
        cliente.setEdad(cliente.getEdad()+1);
        return cliente;
    }

}
