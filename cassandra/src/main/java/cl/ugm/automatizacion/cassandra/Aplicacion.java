package cl.ugm.automatizacion.cassandra;

import com.datastax.driver.core.*;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Base64;
import java.util.Iterator;

public class Aplicacion {

    public static void imprimir(Iterator<Row> it){
        while(it.hasNext()){
            Row row = it.next();
            Integer persona_id = row.get("persona_id", Integer.class);
            String nombre = row.get("nombre", String.class);

            System.out.println(row.toString());
            System.out.println("Persona: "+persona_id+" "+nombre);
        }

    }

    public static void main(String[] args) throws Exception {
        Cluster cluster = Cluster.builder()
                .addContactPoint("localhost")
                .withPort(9042)
                .build();

         FileInputStream fi = new FileInputStream("apache_nifi.pptx");
        byte[] buffer = new byte[fi.available()];
        fi.read(buffer);
        fi.close();

        byte[] encoded = Base64.getEncoder().encode(buffer);
        Session session = cluster.connect();

        session.execute("Create keyspace if not exists automatizacion with replication = {'class':'SimpleStrategy','replication_factor':1}");

        session.execute("Drop Table if exists automatizacion.persona");
        session.execute("create Table automatizacion.persona (persona_id int, nombre text, PRIMARY KEY (persona_id))");


        System.out.println("Select");
        ResultSet rs = session.execute("select * from automatizacion.persona");
        imprimir(rs.iterator());


        System.out.println("Insert");
        rs = session.execute("insert into automatizacion.persona (persona_id, nombre) values (2, 'Hermes')");
        imprimir(rs.iterator());

        System.out.println("Update");
        rs = session.execute("update automatizacion.persona set nombre='Nemesio' where persona_id=1");
        imprimir(rs.iterator());
        System.out.println(rs.wasApplied());

        System.out.println("Delete");
        rs = session.execute("delete from contabilidad.persona where persona_id=10");
        imprimir(rs.iterator());

        PreparedStatement ps = session.prepare("insert into automatizacion.persona (persona_id, nombre) values (20, ?)");
        BoundStatement bs = ps.bind(new String(encoded));
        session.execute(bs);


        rs = session.execute("select nombre from automatizacion.persona where persona_id=20");

        byte[] decoded = Base64.getDecoder().decode(rs.one().get("nombre", String.class));
        FileOutputStream fo = new FileOutputStream("archivo.pptx");
        fo.write(decoded);
        fo.close();



        System.out.println(rs.wasApplied());
        session.close();
        cluster.close();

    }
}
