
ZOOKEEPER START
bin/zookeeper-server-start.sh config/zookeeper.properties

BROKER START
bin/kafka-server-start.sh config/server.properties

TOPIC CREATION
bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-plaintext-input

bin/kafka-topics.sh --create --zookeeper localhost:2181 --replication-factor 1 --partitions 1 --topic streams-wordcount-output --config cleanup.policy=compact

SHOW TOPICS
bin/kafka-topics.sh --zookeeper localhost:2181 --describe

RUN WORDCOUNT DEMO
bin/kafka-run-class.sh org.apache.kafka.streams.examples.wordcount.WordCountDemo

PRODUCER FOR RUN WORDCOUNT DEMO
bin/kafka-console-producer.sh --broker-list localhost:9092 --topic streams-plaintext-input

CONSUMER FOR RUN WORDCOUNT DEMO
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 \
    --topic streams-wordcount-output \
    --from-beginning \
    --formatter kafka.tools.DefaultMessageFormatter \
    --property print.key=true \
    --property print.value=true \
    --property key.deserializer=org.apache.kafka.common.serialization.StringDeserializer \
    --property value.deserializer=org.apache.kafka.common.serialization.LongDeserializer
