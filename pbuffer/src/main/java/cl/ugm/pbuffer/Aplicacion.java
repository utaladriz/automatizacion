package cl.ugm.pbuffer;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class Aplicacion {

    public static void main(String[] args) throws Exception {
        ClienteP.Cliente mensaje = ClienteP.Cliente.newBuilder()
                .setNombre("Mathieu")
                .setEdad(23)
                .build();
        byte[] buffer = mensaje.toByteArray();
        FileOutputStream salida = new FileOutputStream("mensaje.dat");
        salida.write(buffer);
        salida.close();

        FileInputStream entrada = new FileInputStream("mensaje.dat");
        buffer = new byte[entrada.available()];
        entrada.read(buffer);
        entrada.close();
        mensaje = ClienteP.Cliente.parseFrom(buffer);
        System.out.println(mensaje.getNombre()+" "+mensaje.getEdad());

    }
}
